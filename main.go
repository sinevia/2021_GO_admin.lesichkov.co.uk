package main

import (
	"fmt"
	"log"

	"lesichkov.co.uk/app/db"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/template/django"
	"github.com/gouniverse/utils"
	"lesichkov.co.uk/app/routes"
)

func main() {
	// router := prepareRouter()

	// appAddress := helpers.AppURL() + ":" + helpers.AppPort()

	// log.Printf("Lesichkov.co.uk: listening on http://%s", appAddress)
	// log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", helpers.AppPort()), router))

	log.Println("1. Initializing environment variables...")
	utils.EnvInitialize()

	log.Println("2. Initializing database variables...")
	db.Initialize()

	log.Println("3. Initializing application...")
	app := appInitialize()

	log.Println("4. Setting routes...")
	routes.SetupRoutes(app)

	//articles := db.ArticleList(0, 20, "", "created", "desc")
	//log.Println(articles)

	log.Println("5. Starting server on port " + utils.AppPort() + "...")
	log.Fatal(app.Listen(":" + utils.AppPort()))
}

// func prepareRouter() *chi.Mux {
// 	r := chi.NewRouter()

// 	r.Use(middleware.RequestID)
// 	r.Use(middleware.RealIP)
// 	r.Use(middleware.Logger)
// 	r.Use(middleware.Recoverer)
// 	r.Use(middleware.Timeout(60 * time.Second))

// 	r.Get("/auth/login", apphttpauth.Login)
// 	r.Post("/auth/login", apphttpauth.Login)
// 	r.Get("/auth/login/", apphttpauth.Login)
// 	r.Post("/auth/login/", apphttpauth.Login)

// 	r.Get("/user/articles/article-list", apphttpuser.ArticleList)
// 	r.Post("/user/articles/article-list", apphttpuser.ArticleList)
// 	r.Get("/user/articles/article-list/", apphttpuser.ArticleList)
// 	r.Post("/user/articles/article-list/", apphttpuser.ArticleList)
// 	r.Get("/user/articles/article-find", apphttpuser.ArticleFind)
// 	r.Post("/user/articles/article-find", apphttpuser.ArticleFind)
// 	r.Get("/user/articles/article-find/", apphttpuser.ArticleFind)
// 	r.Post("/user/articles/article-find/", apphttpuser.ArticleFind)
// 	r.Get("/user/articles/article-update", apphttpuser.ArticleUpdate)
// 	r.Post("/user/articles/article-update", apphttpuser.ArticleUpdate)
// 	r.Get("/user/articles/article-update/", apphttpuser.ArticleUpdate)
// 	r.Post("/user/articles/article-update/", apphttpuser.ArticleUpdate)

// 	r.Get("/", apphttp.Home)
// 	r.Post("/", apphttp.Home)

// 	return r
// }

// appInitialize intializes the Fiber application and the template engine
func appInitialize() *fiber.App {
	engine := django.New("./views", ".html")

	engine.AddFunc("scripts", utils.ScriptsHTML)
	engine.AddFunc("styles", utils.StylesHTML)
	// engine.AddFunc("statcounter", helpers.StatCounter)

	// Reload the templates on each render, good for development
	// engine.Reload(true) // Optional. Default: false

	// Debug will print each template that is parsed, good for debugging
	if utils.AppEnv() != "production" {
		engine.Debug(true) // Optional. Default: false
	}

	// Layout defines the variable name that is used to yield templates within layouts
	//engine.Layout("embed") // Optional. Default: "embed"

	// Delims sets the action delimiters to the specified strings
	//engine.Delims("{{", "}}") // Optional. Default: engine delimiters

	app := fiber.New(fiber.Config{
		Views: engine,
		// Override default error handler
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			// Statuscode defaults to 500
			code := fiber.StatusInternalServerError

			// Retreive the custom statuscode if it's an fiber.*Error
			if e, ok := err.(*fiber.Error); ok {
				code = e.Code
				log.Println(e.Message)
			}

			// Print code
			log.Println(code)

			// Send custom error page
			if utils.FileExists(fmt.Sprintf("./views/errors/%d.html", code)) {
				return ctx.Status(code).Render(fmt.Sprintf("errors/%d", code), fiber.Map{
					"AppName": utils.AppName(),
				})
			}

			if err != nil {
				//In case the SendFile fails
				log.Println(err)
				return ctx.Status(500).SendString("Internal Server Error")
			}

			// Return from handler
			return nil
		},
	})

	return app
}
