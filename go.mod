module lesichkov.co.uk

go 1.14

require (
	github.com/Masterminds/squirrel v1.4.0
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
	github.com/darkoatanasovski/htmltags v1.0.0
	github.com/emirpasic/gods v1.12.0
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofiber/adaptor/v2 v2.1.1
	github.com/gofiber/fiber/v2 v2.2.5
	github.com/gofiber/template v1.6.6
	github.com/gouniverse/api v1.2.0
	github.com/gouniverse/password v1.1.0
	github.com/gouniverse/sql v0.0.0-20201002074453-daabcb4319ae
	github.com/gouniverse/uid v1.1.0
	github.com/gouniverse/utils v1.4.11
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/jordan-wright/email v4.0.1-0.20200917010138-e1c00e156980+incompatible
	github.com/klauspost/compress v1.11.3 // indirect
	github.com/lesichkovm/gouid v1.0.0
	github.com/tdewolff/minify/v2 v2.9.10 // indirect
	github.com/tdewolff/parse/v2 v2.5.6 // indirect
	golang.org/x/net v0.0.0-20201209123823-ac852fbbde11 // indirect
	golang.org/x/sys v0.0.0-20201211090839-8ad439b19e0f // indirect
	golang.org/x/text v0.3.4 // indirect
	gorm.io/driver/mysql v1.0.3
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.20.8
)
