/*
 * If not user, redirect to guest home
 */
$(function () {
  // Is the user fine? No, redirect to start page
  if ($$.getUser() === null) {
    return $$.to("/");
  }

  // Is the user token fine? No, redirect to start page
  if ($$.getToken() === null) {
    return $$.to("/");
  }

  // Is the user Active? No, redirect to start page
  if ($$.getUser().Status !== "Active") {
    return $$.to("/");
  }

  // Is there an Active subscriptions? No, redirect to subscribe
  // if ($.trim($$.getUser().SubscriptionId) == "") {
  //   if ($$.getUrl().indexOf("subscribe") < 0) {
  //     return $$.to("/user/subscribe");
  //   }
  // }
});
