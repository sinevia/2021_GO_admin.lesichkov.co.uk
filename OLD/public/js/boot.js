var Boot = {
  debug: true,
};

Boot.loadScriptAsync = async function (url) {
  if (url.trim() === "") {
    return;
  }
  return new Promise((resolve, reject) => {
    var script = document.createElement("script");
    script.onload = () => resolve();
    //script.onerror = reject;
    script.src = url;
    document.head.appendChild(script); //or something of the likes
  });
};

Boot.loadScripts = async function (scripts, cb) {
  for (var i = 0; i < scripts.length; i++) {
    await Boot.loadScriptAsync(scripts[i]);
  }
  if (typeof cb === "function") {
    cb();
  }
};

Boot.loadStyleAsync = async function (url) {
  if (url.trim() === "") {
    return;
  }
  return new Promise((resolve, reject) => {
    var link = document.createElement("link");
    link.onload = () => resolve();
    //script.onerror = reject;
    link.href = url;
    link.type = "text/css";
    link.rel = "stylesheet";
    document.head.appendChild(link); //or something of the likes
  });
};

Boot.loadStyles = async function (styles) {
  for (var i = 0; i < styles.length; i++) {
    await Boot.loadStyleAsync(styles[i]);
  }
};

Boot.setFavicon = function (url) {
  var link = document.querySelector("link[rel*='icon']") ||
    document.createElement("link");
  link.type = "image/x-icon";
  link.rel = "shortcut icon";
  link.href = url;
  document.getElementsByTagName("head")[0].appendChild(link);
};

Boot.init = function () {
  var me = document.currentScript;
  var fnToExec = me.getAttribute("data-exec") ?? null;
  var scripts = me.getAttribute("data-scripts") ?? [];
  var styles = me.getAttribute("data-styles") ?? [];
  var favicon = me.getAttribute("data-favicon") ?? "";
  if (typeof styles == "string") {
    styles = styles.split(",").filter((e) => {
      return e != "";
    });
  }
  if (typeof scripts == "string") {
    scripts = scripts.split(",").filter((e) => {
      return e != "";
    });
  }

  if (Boot.debug) {
    console.log("Booting...");
  }

  if (favicon.length > 0) {
    if (Boot.debug) {
      console.log("Setting favicon...");
    }
    Boot.setFavicon(favicon);
  }

  if (styles.length > 0) {
    if (Boot.debug) {
      console.log("Setting styles...");
    }
    Boot.loadStyles(styles);
  }
  if (scripts.length > 0) {
    if (typeof eval(fnToExec) === "function") {
      if (Boot.debug) {
        console.log("Setting scripts with callback '" + fnToExec + "'...");
      }
      Boot.loadScripts(scripts, eval(fnToExec));
    } else {
      if (Boot.debug) {
        console.log("Setting scripts with no callback");
      }
      Boot.loadScripts(scripts, null);
    }
  }
};

Boot.init();

// Boot.loadStyles([
//   "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css",
//   "//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.css",
// ]);

// Boot.loadScripts(
//   [
//     "https://code.jquery.com/jquery-3.5.1.min.js",
//     "/js/main.js",
//     "https://cdn.jsdelivr.net/gh/lesichkovm/web@2.5.0/initialize.js",
//     "https://unpkg.com/vue",
//     // "https://unpkg.com/vue@next",
//     "https://unpkg.com/http-vue-loader",
//     "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js",
//     "//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.js",
//   ],
//   eval(fnToExec),
// );

// Boot.setFavicon(
//   "data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAD9/f0AAAAAAP///wD4+PgA8fHxAPr6+gD8/PwA7u7uAP7+/gDw8PAA+fn5APLy8gD7+/sADAzoAPT09AAAAAAAh4cRGHERuYfq4d3RHd0T6nJx0R3RHRByJxEd0R3REYdxeBERERFQEiEnEREREbkXchEZLdchEXInESYScXIRh3F4eRchJ1ASYWFqRkZGGxTLEcERERwRy3gRHYeH0RF4JyHd3d3dGYdycd3d3d0QcicnHd3d0bmHcnhxEREXUHIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
// );
