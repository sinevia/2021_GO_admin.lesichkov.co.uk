/*
 * If not guest, redirect to user dashboard
 */
$(function () {
  if ($$.getUser() !== null && $$.getToken() !== null) {
    if ($$.getUser().Status == "active") {
      if ($$.getUrl().indexOf("logout") < 0) {
        $$.to("user/home");
      }
    }
  }
});
