Boot.loadStyles([
  "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css",
  "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css",
  "//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.css",
  "//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css",
  "/css/user.css",
]);

Boot.loadScripts(
  [
    "https://cdn.jsdelivr.net/npm/pace-js@1.0.2/pace.min.js",
    "https://code.jquery.com/jquery-3.5.1.min.js",
    "/js/main.js",
    //"https://cdn.jsdelivr.net/gh/lesichkovm/web@2.5.0/initialize.js",
    "/js/web.js",
    "/js/authenticate-user.js",
    "https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.12/vue.min.js",
    //"https://unpkg.com/http-vue-loader",
    "/js/http-vue-loader-142.js",
    "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js",
    "//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.js",
    "https://cdn.jsdelivr.net/npm/sweetalert2@9",
    "//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js",
  ],
  userInit,
);

Boot.setFavicon(
  "data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAD9/f0AAAAAAP///wD4+PgA8fHxAPr6+gD8/PwA7u7uAP7+/gDw8PAA+fn5APLy8gD7+/sADAzoAPT09AAAAAAAh4cRGHERuYfq4d3RHd0T6nJx0R3RHRByJxEd0R3REYdxeBERERFQEiEnEREREbkXchEZLdchEXInESYScXIRh3F4eRchJ1ASYWFqRkZGGxTLEcERERwRy3gRHYeH0RF4JyHd3d3dGYdycd3d3d0QcicnHd3d0bmHcnhxEREXUHIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
);

function userInit() {
  /* START: Check token */
  $(document).ajaxSuccess(function (evt, jqXHR, settings) {
    var json = jqXHR.responseJSON;
    if (typeof json !== "undefined") {
      var status = json.status.toLowerCase();
      if (
        status === "authenticationfailure" ||
        status === "authentication_failed" ||
        status === "unauthenticated" ||
        status === "unauthorized"
      ) {
        var message = json.message.toLowerCase();
        $$.setUser(null);
        $$.setToken(null);
        Registry.set("LoginError", message);
        //$$.to('guest/auth/login.html?message=' + message);
        $$.to("/auth/login.html?message=" + message);
      }
    }
  });
  /* END: Check token */

  main();
}
