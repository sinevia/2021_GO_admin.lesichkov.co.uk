package routes

import (
	"log"
	"strings"

	valid "github.com/asaskevich/govalidator"
	"github.com/gofiber/fiber/v2"
	"github.com/gouniverse/password"
	"github.com/gouniverse/uid"
	"github.com/gouniverse/utils"
	"lesichkov.co.uk/app/db"
	"lesichkov.co.uk/app/helpers"
)

// SetupAuthRoutes setups the auth URLs
func SetupAuthRoutes(endpoint string, app *fiber.App) {
	//api := app.Group("/api", logger.New(), middleware.AuthReq())
	api := app.Group(endpoint)

	// Auth
	auth := api.Group("/auth")
	auth.Post("/email-verify", authEmailVerify)
	auth.Post("/login", authLogin)
	auth.Post("/password-restore", authPasswordRestore)
	auth.Post("/register", authRegister)
	auth.Post("/password-change", authPasswordChange)
}

func authEmailVerify(c *fiber.Ctx) error {
	token := c.FormValue("token")

	cache := db.CacheFindByKey(token)

	if cache == nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "This link is invalid or has expired",
		})
	}

	email := cache.Value

	user := db.UserFindByEmail(email)

	if user == nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "E-mail not registered",
		})
	}

	user.Status = db.UserStatusActive
	//db.GetDb().Save(&user)

	dbResult := db.GetDb().Save(&user)

	if dbResult.Error != nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "E-mail verification failed. IO Error",
		})
	}

	return c.JSONP(fiber.Map{
		"status":  "success",
		"message": "Your email was verified you may now login",
	})
}

func authLogin(c *fiber.Ctx) error {
	email := c.FormValue("email")
	pass := c.FormValue("password")

	if email == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Email is required field",
		})
	}

	if pass == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Password is required field",
		})
	}

	user := db.UserFindByEmail(email)
	if user == nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "User not found: " + email,
			"data":    fiber.Map{},
		})
	}

	if password.Verify(pass, user.Password) == false {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Password does not match",
			"data":    fiber.Map{},
		})
	}

	authToken := utils.RandStr(16)

	db.CacheSet(authToken, user.ID, 3600)

	return c.JSONP(fiber.Map{
		"status": "success",
		"data": fiber.Map{
			"user":       user,
			"auth_token": authToken,
		},
	})
}

func authPasswordChange(c *fiber.Ctx) error {
	token := c.FormValue("token")
	pass := c.FormValue("password")
	passConfirm := c.FormValue("password2")

	if token == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Token is required",
		})
	}

	if pass == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "New Password is required field",
		})
	}

	if pass != passConfirm {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Password and password confirmation do not match",
		})
	}

	cache := db.CacheFindByKey(token)

	if cache == nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "This link is invalid or has expired",
		})
	}

	email := cache.Value

	user := db.UserFindByEmail(email)

	if user == nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "E-mail not registered",
		})
	}

	passwordHash, err := password.Hash(pass)

	if err != nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Error hashing password",
		})
	}

	user.Password = passwordHash

	dbResult := db.GetDb().Save(&user)

	if dbResult.Error != nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Error changing your password. Try again later...",
		})
	}

	return c.JSONP(fiber.Map{
		"status":  "success",
		"message": "Password changed you may now login",
	})

}

func authPasswordRestore(c *fiber.Ctx) error {
	email := c.FormValue("email")
	firstName := c.FormValue("first_name")

	if email == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Email is required field",
		})
	}

	if firstName == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "First name is required field",
		})
	}

	user := db.UserFindByEmail(email)

	if user == nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "E-mail not registered",
		})
	}

	if strings.ToLower(user.FirstName) != strings.ToLower(firstName) {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "First name not matching",
		})
	}

	emailPasswordChangeToken := uid.NanoUid()
	db.CacheSet(emailPasswordChangeToken, user.Email, 3600)

	emailContent := helpers.EmailPasswordChangeTemplate(user.Email, helpers.LinkAuthPasswordChange(emailPasswordChangeToken))

	isSent, err := helpers.EmailSend(helpers.EmailFromAddress(), []string{user.Email}, "Password Restore", emailContent)

	log.Println(err)

	if isSent {
		return c.JSONP(fiber.Map{
			"status":  "success",
			"message": "Password reset link was sent to your e-mail",
		})
	}

	return c.JSONP(fiber.Map{
		"status":  "error",
		"message": "Password reset link failed to be sent. Please try again later",
	})
}

func authRegister(c *fiber.Ctx) error {
	firstName := c.FormValue("first_name")
	lastName := c.FormValue("last_name")
	email := c.FormValue("email")
	pass := c.FormValue("password")

	if firstName == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "First name is required field",
		})
	}
	if lastName == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Last name is required field",
		})
	}

	if email == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Email is required field",
		})
	}

	if pass == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Password is required field",
		})
	}

	if valid.IsEmail(email) == false {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Email is invalid format",
		})
	}

	existingUser := db.UserFindByEmail(email)
	if existingUser != nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "This email is already registered",
			"data":    fiber.Map{},
		})
	}

	passwordHash, err := password.Hash(pass)

	if err != nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Error hashing password",
		})
	}

	var newUser = db.User{FirstName: firstName, LastName: lastName, Email: email, Password: passwordHash, Status: db.UserStatusEmailUnverified}

	dbResult := db.GetDb().Create(&newUser)

	if dbResult.Error != nil {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Error creating account",
		})
	}

	emailVerificationToken := uid.NanoUid()
	db.CacheSet(emailVerificationToken, newUser.Email, 3600)
	emailVerificationURL := helpers.LinkAuthEmailVerify(emailVerificationToken)

	to := []string{newUser.Email}
	msg := helpers.EmailVerificationTemplate(newUser.FirstName+" "+newUser.LastName, emailVerificationURL)
	isSent, _ := helpers.EmailSend("info@sinevia.com", to, "Account Verification", msg)

	if isSent == false {
		return c.JSONP(fiber.Map{
			"status":  "success",
			"message": "Account successfully created",
		})
	}

	return c.JSONP(fiber.Map{
		"status":  "success",
		"message": "Please check your inbox for the verification email sent",
	})

}
