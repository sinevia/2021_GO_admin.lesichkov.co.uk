package routes

import (
	"log"
	"path/filepath"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/gouniverse/utils"
	"lesichkov.co.uk/app/db"
)

func AuthMiddleware(c *fiber.Ctx) error {
	token := strings.Trim(c.FormValue("_auth"), " ")

	// 1. Is token provided?
	if token == "" {
		return c.JSONP(fiber.Map{
			"status":  "error",
			"message": "Token is required field",
		})
	}

	// 2. Is token valid?
	cache := db.CacheFindByKey(token)

	if cache == nil {
		return c.JSONP(fiber.Map{
			"status":  "unauthenticated",
			"message": "Authentication failed",
		})
	}

	// Go to next middleware:
	return c.Next()
}

// SetupRoutes func
func SetupRoutes(app *fiber.App) {
	SetupAuthRoutes("/api", app)
	SetupArticleRoutes("/api/user/articles", app, AuthMiddleware)
	app.Get("/*", dynamicPage)
	app.Get("/", func(ctx *fiber.Ctx) error {
		return ctx.Send([]byte("Hello"))
	})
}

// dynamicPage shows the website pages if .html exists for it
func dynamicPage(c *fiber.Ctx) error {
	page := c.Params("*")
	extension := filepath.Ext(page)

	if extension == ".ico" {
		return c.SendStatus(404) // => 404 "Not Found"
	}

	if page == "" {
		page = "index"
	}

	log.Println("Page: " + page)

	path := page + ".html"

	// Does page exists?
	if utils.FileExists("views/"+path) == false {
		log.Println("View path: views/" + path + " not found")
		//return c.SendStatus(404) // => 404 "Not Found"
		return c.Status(404).Render("errors/404", fiber.Map{
			"AppName": utils.AppName(),
		})
	}

	// Render page
	log.Println(ips(c))
	return c.Render(page, fiber.Map{
		"AppName": utils.AppName(),
		"Ips":     ips(c),
	})

}

func ips(c *fiber.Ctx) []string {
	ips := c.IPs()
	ips = append(ips, c.IP())
	return ips
}
