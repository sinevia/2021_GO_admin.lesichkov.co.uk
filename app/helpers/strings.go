package helpers

import (
	"crypto/rand"
	"encoding/base64"
)

// RandStr generates random string of specified length
func RandStr(len int) string {
	buff := make([]byte, len)
	rand.Read(buff)
	str := base64.StdEncoding.EncodeToString(buff)
	// Base 64 can be longer than len
	return str[:len]
}
