package helpers

import (
	"encoding/base64"
	"log"
	"net/http"
	"os"
	"time"
)

// AppAddress return the full URL and PORT for the app
func AppAddress() string {
	appAddress := os.Getenv("APP_URL") + ":" + os.Getenv("APP_PORT")
	return appAddress
}

// AppEnv returns the environment the app is running in
func AppEnv() string {
	appEnv := os.Getenv("APP_ENV")
	if appEnv == "live" || appEnv == "production" {
		return "live"
	}
	if appEnv == "development" || appEnv == "dev" {
		return "development"
	}
	return appEnv
}

// AppName return the name for the app
func AppName() string {
	appName := os.Getenv("APP_NAME")
	return appName
}

// AppPort return the port for the app
func AppPort() string {
	appPort := os.Getenv("APP_PORT")
	if appPort == "" {
		appPort = "8080"
	}
	return appPort
}

// AppURL return the URL for the app
func AppURL() string {
	appURL := os.Getenv("APP_URL")
	return appURL
}

// DatabaseDriver return the name for the app
func DatabaseDriver() string {
	value := os.Getenv("DB_DRIVER")
	return value
}

// DatabaseAPIURL return the name for the app
func DatabaseAPIURL() string {
	value := os.Getenv("DB_API_URL")
	return value
}

// DatabaseAPIKEY return the name for the app
func DatabaseAPIKEY() string {
	value := os.Getenv("DB_API_KEY")
	return value
}

// EmailFromAddress return the URL for the app
func EmailFromAddress() string {
	emailFromAddress := os.Getenv("EMAIL_FROM_ADDRESS")
	return emailFromAddress
}

// EmailFromName return the URL for the app
func EmailFromName() string {
	emailFromName := os.Getenv("EMAIL_FROM_NAME")
	return emailFromName
}

// SetFlash sets a flash message
func SetFlash(w http.ResponseWriter, name string, value string) {
	//expiration := time.Now().Add(365 * 24 * time.Hour)
	expiration := time.Now().Add(1 * time.Minute)
	cookie := &http.Cookie{Name: name, Value: encode([]byte(value)), Expires: expiration}
	http.SetCookie(w, cookie)
}

// GetFlash gets a flash message, and deletes it
func GetFlash(w http.ResponseWriter, r *http.Request, name string) string {
	c, err := r.Cookie(name)
	if err != nil {
		log.Println(err)
		switch err {
		case http.ErrNoCookie:
			return ""
		default:
			return ""
		}
	}
	value, err := decode(c.Value)
	if err != nil {
		log.Println(err)
		return ""
	}
	expiration := time.Now().Add(-365 * 24 * time.Hour)
	cookie := &http.Cookie{Name: name, MaxAge: -1, Expires: expiration}
	http.SetCookie(w, cookie)
	return string(value)
}

// -------------------------

func encode(src []byte) string {
	return base64.URLEncoding.EncodeToString(src)
}

func decode(src string) ([]byte, error) {
	return base64.URLEncoding.DecodeString(src)
}

// FileExists checks if a file exists
func FileExists(filePath string) bool {
	_, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		return false
	}
	return true
}

// // NewTimeoutClient - create http client with TimeOut and disabled http/2
// func NewTimeoutClient(args ...interface{}) *http.Client {
// 	// Default configuration
// 	config := &Config{
// 		ConnectTimeout:   5 * time.Second,
// 		ReadWriteTimeout: 5 * time.Second,
// 	}

// 	// merge the default with user input if there is one
// 	if len(args) == 1 {
// 		timeout := args[0].(time.Duration)
// 		config.ConnectTimeout = timeout
// 		config.ReadWriteTimeout = timeout
// 	}

// 	if len(args) == 2 {
// 		config.ConnectTimeout = args[0].(time.Duration)
// 		config.ReadWriteTimeout = args[1].(time.Duration)
// 	}
// 	http.DefaultTransport.(*http.Transport).TLSNextProto = make(map[string]func(string, *tls.Conn) http.RoundTripper)

// 	return &http.Client{
// 		Transport: &http.Transport{
// 			Dial: TimeoutDialer(config),
// 		},
// 	}
// }

// // HTTPGetBody create get request with default headers
// // return nil or data
// func HTTPGetBody(url string) []byte {

// 	client := NewTimeoutClient()
// 	req, err := http.NewRequest("GET", url, nil)
// 	if err != nil {
// 		return nil
// 	}
// 	for k, v := range defHeaders {
// 		req.Header.Set(k, v)
// 	}

// 	resp, err := client.Do(req)
// 	if err != nil {
// 		return nil
// 	}
// 	defer resp.Body.Close()
// 	if resp.StatusCode != 200 {
// 		return nil
// 	}
// 	body, err := ioutil.ReadAll(resp.Body)
// 	if err == nil {
// 		return body
// 	}

// 	return nil
// }

// // checkAndCreate may create dirs
// func CheckAndCreate(path string) (bool, error) {
// 	// detect if file exists
// 	var _, err = os.Stat(path)
// 	if err == nil {
// 		return true, err
// 	}
// 	// create dirs if file not exists
// 	if os.IsNotExist(err) {
// 		if filepath.Dir(path) != "." {
// 			return false, os.MkdirAll(filepath.Dir(path), 0777)
// 		}
// 	}
// 	return false, err
// }

// func HTTPPostJson(url string, headers map[string]string, json []byte) []byte {

// 	client := NewTimeoutClient(1 * time.Second)
// 	req, err := http.NewRequest("POST", url, bytes.NewBuffer(json))
// 	if err != nil {
// 		return nil
// 	}
// 	for k, v := range headers {
// 		req.Header.Set(k, v)
// 	}

// 	resp, err := client.Do(req)
// 	if err != nil {
// 		return nil
// 	}
// 	defer resp.Body.Close()
// 	if resp.StatusCode != 200 {
// 		return nil
// 	}
// 	body, err := ioutil.ReadAll(resp.Body)
// 	if err == nil {
// 		return body
// 	}

// 	return nil
// }
