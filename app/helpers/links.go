package helpers

import "os"

// LinkWebsite returns a URL to the main website
func LinkWebsite() string {
	url := AppURL()
	port := AppPort()

	if url == "localhost" {
		return "http://" + url + ":" + port
	}

	return "https://" + url
}

// LinkAPI returns a URL to the API
func LinkAPI() string {
	appURL := os.Getenv("APP_URL")
	appPort := os.Getenv("APP_PORT")
	appAddress := appURL + ":" + appPort
	return "https://" + appAddress
}

// LinkAuthEmailVerify  returns a URL to the verification email endpoint
func LinkAuthEmailVerify(token string) string {
	return LinkWebsite() + "/auth/email-verify.html?token=" + token
}

// LinkAuthLogin  returns a URL to the login endpoint
func LinkAuthLogin() string {
	return LinkWebsite() + "/auth/login.html"
}

// LinkAuthLogout  returns a URL to the logout endpoint
func LinkAuthLogout() string {
	return LinkWebsite() + "/auth/logout.html"
}

// LinkAuthPasswordChange  returns a URL to the verification email endpoint
func LinkAuthPasswordChange(token string) string {
	return LinkWebsite() + "/auth/password-change.html?token=" + token
}

// LinkAuthPasswordRestore  returns a URL to the verification email endpoint
func LinkAuthPasswordRestore(token string) string {
	return LinkWebsite() + "/auth/password-restore.html?token=" + token
}

// LinkAuthRegister  returns a URL to the verification email endpoint
func LinkAuthRegister() string {
	return LinkWebsite() + "/auth/register.html"
}

// LinkGuestHome  returns a URL to the login endpoint
func LinkGuestHome() string {
	return LinkWebsite() + ""
}

// LinkUserHome  returns a URL to the login endpoint
func LinkUserHome() string {
	return LinkWebsite() + "/user"
}
