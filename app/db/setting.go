package db

import (
	"errors"
	"time"

	"github.com/gouniverse/uid"
	"gorm.io/gorm"
)

// Setting type
type Setting struct {
	ID        string     `gorm:"type:varchar(40);column:id;primary_key;"`
	Key       string     `gorm:"type:varchar(40);column:key;DEFAULT NULL;"`
	Value     string     `gorm:"type:longtext;column:value;"`
	CreatedAt time.Time  `gorm:"type:datetime;column:created_at;DEFAULT NULL;"`
	UpdatedAt time.Time  `gorm:"type:datetime;column:updated_at;DEFAULT NULL;"`
	DeletedAt *time.Time `gorm:"type:datetime;olumn:deleted_at;DEFAULT NULL;"`
}

// TableName the name of the User table
func (Setting) TableName() string {
	return "snv_settings_setting"
}

// BeforeCreate adds UID to model
func (c *Setting) BeforeCreate(tx *gorm.DB) (err error) {
	uuid := uid.NanoUid()
	c.ID = uuid
	return nil
}

// SettingDelete removes all keys from the sessiom
func SettingDelete(key string) bool {
	session := SettingFindByKey(key)

	if session == nil {
		return true
	}

	GetDb().Delete(&session)

	return true
}

// SettingFindByKey finds a session by key
func SettingFindByKey(key string) *Setting {
	setting := &Setting{}
	result := GetDb().Where("`key` = ?", key).First(&setting)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil
	}

	return setting
}

// SettingGet gets a key from cache
func SettingGet(key string, valueDefault string) string {
	setting := SettingFindByKey(key)

	if setting != nil {
		return setting.Value
	}

	return valueDefault
}

// SettingSet sets a key in cache
func SettingSet(key string, value string) bool {
	setting := SettingFindByKey(key)

	if setting != nil {
		setting.Value = value
		dbResult := GetDb().Save(&setting)
		if dbResult != nil {
			return false
		}
		return true
	}

	var newSetting = Setting{Key: key, Value: value}

	dbResult := GetDb().Create(&newSetting)

	if dbResult.Error != nil {
		return false
	}

	return true
}
