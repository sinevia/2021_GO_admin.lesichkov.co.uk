package db

import (
	"encoding/json"
	"time"

	"github.com/gouniverse/uid"
	"gorm.io/gorm"
)

const (
	EntityStatusActive   = "active"
	EntityStatusInactive = "inactive"
)

// Entity type
type Entity struct {
	ID          string     `gorm:"type:varchar(40);column:id;primary_key;"`
	Status      string     `gorm:"type:varchar(10);column:status;"`
	Type        string     `gorm:"type:varchar(40);column:type;"`
	Name        string     `gorm:"type:varchar(255);column:name;DEFAULT NULL;"`
	Description string     `gorm:"type:longtext;column:description;"`
	CreatedAt   time.Time  `gorm:"type:datetime;column:created_at;DEFAULT NULL;"`
	UpdatedAt   time.Time  `gorm:"type:datetime;column:updated_at;DEFAULT NULL;"`
	DeletedAt   *time.Time `gorm:"type:datetime;olumn:deleted_at;DEFAULT NULL;"`

	Attributes []EntityAttribute
}

// TableName teh name of the User table
func (Entity) TableName() string {
	return "snv_entities_entity"
}

// BeforeCreate adds UID to model
func (e *Entity) BeforeCreate(tx *gorm.DB) (err error) {
	uuid := uid.NanoUid()
	e.ID = uuid
	return nil
}

// EntityAttribute type
type EntityAttribute struct {
	ID             string     `gorm:"type:varchar(40);column:id;primary_key;"`
	EntityID       string     `gorm:"type:varchar(40);column:entity_id;"`
	AttributeKey   string     `gorm:"type:varchar(255);column:attribute_key;DEFAULT NULL;"`
	AttributeValue string     `gorm:"type:longtext;column:column:attribute_value;"`
	CreatedAt      time.Time  `gorm:"type:datetime;column:created_at;DEFAULT NULL;"`
	UpdatedAt      time.Time  `gorm:"type:datetime;column:updated_at;DEFAULT NULL;"`
	DeletedAt      *time.Time `gorm:"type:datetime;olumn:deleted_at;DEFAULT NULL;"`
}

// TableName teh name of the User table
func (EntityAttribute) TableName() string {
	return "snv_entities_attribute"
}

// BeforeCreate adds UID to model
func (e *EntityAttribute) BeforeCreate(tx *gorm.DB) (err error) {
	uuid := uid.NanoUid()
	e.ID = uuid
	return nil
}

// SetValue serializes the values
func (e *EntityAttribute) SetValue(value interface{}) bool {
	bytes, err := json.Marshal(value)

	if err != nil {
		return false
	}

	e.AttributeValue = string(bytes)

	return true
}

// GetValue serializes the values
func (e *EntityAttribute) GetValue() interface{} {
	var value interface{}
	err := json.Unmarshal([]byte(e.AttributeValue), &value)

	if err != nil {
		panic("JSOB error unmarshaliibg attribute" + err.Error())
	}

	return value
}

// EntityCreate func
func EntityCreate(entityType string) *Entity {
	entity := &Entity{Type: entityType, Status: EntityStatusActive}

	dbResult := GetDb().Create(&entity)

	if dbResult.Error != nil {
		return nil
	}

	return entity
}

// EntityCreateWithAttributes func
func EntityCreateWithAttributes(entityType string, attributes map[string]interface{}) *Entity {
	entityAttributes := make([]EntityAttribute, 0)
	for k, v := range attributes {
		// bytes, err := json.Marshal(v)

		// if err != nil {
		// 	return nil
		// }

		// value := string(bytes)
		ea := EntityAttribute{AttributeKey: k} //, AttributeValue: value}
		ea.SetValue(v)
		entityAttributes = append(entityAttributes, ea)
		// val := columnPointers[i].(*interface{})
		// if *val == nil {
		// 	mappedRow[colName] = ""
		// } else {
		// 	str := fmt.Sprintf("%s", *val)
		// 	mappedRow[colName] = str
		// }
	}

	// db.Create(&Dog{Name: "dog1", Toy: []Toy{{Name: "toy1"}, {Name: "toy2"}}})

	entity := &Entity{Type: entityType, Status: EntityStatusActive, Attributes: entityAttributes}

	dbResult := GetDb().Create(&entity)

	if dbResult.Error != nil {
		return nil
	}

	return entity

}
