package db

import (
	"errors"
	"time"

	"github.com/Masterminds/squirrel"
	"gorm.io/gorm"
)

// TableArticle name of the article table
const TableArticle = "snv_articles_article"

// TableArticleTranslation name of the article translation table
const TableArticleTranslation = "snv_articles_articletranslation"

// Article type
type Article struct {
	ID          string     `gorm:"type:varchar(40);column:Id;primary_key;"`
	Status      string     `gorm:"type:varchar(40);column:Status;DEFAULT NULL;"`
	PositionID  string     `gorm:"type:varchar(40);column:PositionId;"`
	CategoryID  string     `gorm:"type:varchar(40);column:CategoryId;"`
	AuthorID    string     `gorm:"type:varchar(40);column:AuthorId;"`
	Wysiwyg     string     `gorm:"type:varchar(40);column:Wysiwyg;"`
	PublishedAt *time.Time `gorm:"type:datetime;column:Published;DEFAULT NULL;"`
	CreatedAt   time.Time  `gorm:"type:datetime;column:Created;DEFAULT NULL;"`
	UpdatedAt   time.Time  `gorm:"type:datetime;column:Updated;DEFAULT NULL;"`
	DeletedAt   *time.Time `gorm:"type:datetime;column:Deleted;DEFAULT NULL;"`
}

// TableName the name of the Article table
func (Article) TableName() string {
	return TableArticle
}

// ArticleTranslation type
type ArticleTranslation struct {
	ID        string `gorm:"type:varchar(40);column:Id;primary_key;"`
	ArticleID string `gorm:"type:varchar(40);column:ArticleId;"`
	Language  string `gorm:"type:varchar(40);column:Language;"`
	Title     string `gorm:"type:varchar(255);column:Title;"`
	Summary   string `gorm:"type:longtext;column:Summary;"`
	Content   string `gorm:"type:longtext;column:Content;"`
	ImageURL  string `gorm:"type:varchar(255);column:ImageUrl;DEFAULT NULL;"`
}

// TableName the name of the Article table
func (ArticleTranslation) TableName() string {
	return TableArticleTranslation
}

// ArticleCreate finds an article by ID
func ArticleCreate(article *Article) *Article {
	//article := &Article{}
	result := GetDb().Save(&article)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil
	}

	return article

	// sqlStr, args, _ := squirrel.Select("*").From(TableArticle).Where(squirrel.Eq{"Id": id}).ToSql()

	// entity := QuerySingle(sqlStr, args...)

	// return entity
}

//ArticleTranslationFindByArticleIDAndLanguage finds translation
func ArticleTranslationFindByArticleIDAndLanguage(articleID string, language string) *ArticleTranslation {
	articleTranslation := &ArticleTranslation{}
	result := GetDb().Where("`ArticleId` = ?", articleID).Where("`Language` = ?", language).First(&articleTranslation)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil
	}

	return articleTranslation
}

func ArticleList(offset uint64, perPage uint64, search string, orderBy string, sort string) []Article {
	articleList := []Article{}
	result := GetDb().Order(orderBy + " " + sort).Offset(int(offset)).Limit(int(perPage)).Find(&articleList)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil
	}

	return articleList
}

// ArticleCount counts articles
func ArticleCount() uint64 {
	var count int64
	GetDb().Model(&Article{}).Count(&count)
	return uint64(count)
	// sqlStr, args, _ := squirrel.Select("COUNT(*) AS count").From(TableArticle).Limit(1).ToSql()

	// entities := Query(sqlStr, args...)

	// count, _ := strconv.ParseUint(entities[0]["count"], 10, 64)

	// return count
}

// ArticleFindByID finds an article by ID
func ArticleFindByID(id string) *Article {
	article := &Article{}
	result := GetDb().Where("`Id` = ?", id).First(&article)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil
	}

	return article

	// sqlStr, args, _ := squirrel.Select("*").From(TableArticle).Where(squirrel.Eq{"Id": id}).ToSql()

	// entity := QuerySingle(sqlStr, args...)

	// return entity
}

// // ArticleList list articles
// func ArticleList(offset uint64, limit uint64, search string, orderBy string, sort string) []map[string]string {
// 	qTranslations := squirrel.Select("ArticleId").From(TableArticleTranslation)

// 	if search != "" {
// 		qTranslations = qTranslations.Where(squirrel.Like{"Title": "%" + search + "%"})
// 	}

// 	sqlStr01, args, _ := qTranslations.ToSql()

// 	translationEntities := Query(sqlStr01, args...)
// 	articleIds := helpers.MapToColumn(translationEntities, "ArticleId")
// 	// articleIds := make([]string, 0)
// 	// for _, element := range translationEntities {
// 	// 	articleIds = append(articleIds, element["ArticleId"])
// 	// }

// 	// log.Println(articleIds)

// 	if search != "" && len(articleIds) < 1 {
// 		return make([]map[string]string, 0)
// 	}

// 	q := squirrel.Select("*").From(TableArticle).OrderBy(orderBy + " " + sort).Offset(offset).Limit(limit)

// 	if len(articleIds) > 0 {
// 		log.Println("with search")
// 		q = q.Where(squirrel.Eq{"Id": articleIds})
// 	}

// 	sqlStr, args, _ := q.ToSql()

// 	log.Println(sqlStr)

// 	entities := Query(sqlStr, args...)

// 	return entities
// }

// // ArticleTranslationFindByID finds a user by email
// func ArticleTranslationFindByID(id string) map[string]string {
// 	sqlStr, args, _ := squirrel.Select("*").From(TableArticle).Where(squirrel.Eq{"Id": id}).ToSql()

// 	entity := QuerySingle(sqlStr, args...)

// 	return entity
// }

// // ArticleTranslationFindByArticleIDAndLanguage finds a user by email
// func ArticleTranslationFindByArticleIDAndLanguage(articleID string, language string) map[string]string {
// 	sqlStr, args, _ := squirrel.Select("*").From(TableArticleTranslation).Where(squirrel.Eq{"ArticleId": articleID}).Where(squirrel.Eq{"Language": language}).ToSql()

// 	entity := QuerySingle(sqlStr, args...)

// 	return entity
// }

// ArticleUpdateByID updates an article by ID
func ArticleUpdateByID(articleID string, data map[string]string) bool {
	mapInterface := make(map[string]interface{})
	for key, value := range data {
		mapInterface[key] = value
	}
	sqlStr, args, _ := squirrel.Update(TableArticle).Where(squirrel.Eq{"Id": articleID}).SetMap(mapInterface).ToSql()

	tx := GetDb().Exec(sqlStr, args...)

	if tx.Error != nil {
		return false
	}

	return true

	// result := Exec(sqlStr, args...)

	// return result
}

// ArticleTranslationUpdate updates an article translation by ID and language
func ArticleTranslationUpdate(articleID string, language string, data map[string]string) bool {
	mapInterface := make(map[string]interface{})
	for key, value := range data {
		mapInterface[key] = value
	}

	sqlStr, args, _ := squirrel.Update(TableArticleTranslation).Where(squirrel.Eq{"ArticleId": articleID}).Where(squirrel.Eq{"Language": language}).SetMap(mapInterface).ToSql()

	tx := GetDb().Exec(sqlStr, args...)

	if tx.Error != nil {
		return false
	}

	return true

	//result := Exec(sqlStr, args...)

	//return result
}
