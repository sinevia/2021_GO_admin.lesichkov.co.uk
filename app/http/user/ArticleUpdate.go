package user

import (
	"net/http"
	"strings"

	"sinevia.com/app/helpers"

	"github.com/gouniverse/api"
	"sinevia.com/app/db"
)

// ArticleUpdate updates the article
func ArticleUpdate(w http.ResponseWriter, r *http.Request) {
	token := strings.Trim(helpers.Req(r, "token", ""), " ")
	articleID := strings.Trim(helpers.Req(r, "article_id", ""), " ")
	status := strings.Trim(helpers.Req(r, "status", ""), " ")
	title := strings.Trim(helpers.Req(r, "title", ""), " ")
	content := strings.Trim(helpers.Req(r, "content", ""), " ")
	wysiwyg := strings.Trim(helpers.Req(r, "wysiwyg", ""), " ")

	if token == "" {
		api.Respond(w, r, api.Error("Token is required field"))
		return
	}

	if db.CacheGet(token, "") == "" {
		api.Respond(w, r, api.Unauthorized("Authentication failed"))
		return
	}

	if articleID == "" {
		api.Respond(w, r, api.Error("article_id is required field"))
		return
	}

	articleFound := db.ArticleFindByID(articleID)

	if articleFound == nil {
		api.Respond(w, r, api.Error("Article not found"))
		return
	}

	if status != "" {
		isSuccess := db.ArticleUpdate(articleID, map[string]string{"Status": status})
		if isSuccess == false {
			api.Respond(w, r, api.Error("Article status failed to be saved"))
			return
		}
	}

	if wysiwyg != "" {
		isSuccess := db.ArticleUpdate(articleID, map[string]string{"Wysiwyg": wysiwyg})
		if isSuccess == false {
			api.Respond(w, r, api.Error("Article wysiwyg failed to be saved"))
			return
		}
	}

	if title != "" {
		isSuccess := db.ArticleTranslationUpdate(articleID, "en", map[string]string{"Title": title})
		if isSuccess == false {
			api.Respond(w, r, api.Error("Article title failed to be saved"))
			return
		}
	}

	if content != "" {
		isSuccess := db.ArticleTranslationUpdate(articleID, "en", map[string]string{"Content": content})
		if isSuccess == false {
			api.Respond(w, r, api.Error("Article content failed to be saved"))
			return
		}
	}

	article := db.ArticleFindByID(articleID)

	translation := db.ArticleTranslationFindByArticleIDAndLanguage(article["Id"], "en")
	if translation == nil {
		api.Respond(w, r, api.Error("Translation not found"))
		return
	}

	dataArticle := map[string]string{}
	dataArticle["id"] = article["Id"]
	dataArticle["status"] = article["Status"]
	dataArticle["wysiwyg"] = article["Wysiwyg"]
	dataArticle["title"] = translation["Title"]
	dataArticle["content"] = translation["Content"]
	dataArticle["created_at"] = article["Created"]
	dataArticle["updated_at"] = article["Updated"]
	dataArticle["published_at"] = article["Published"]

	data := map[string]interface{}{
		"article": dataArticle,
	}

	api.Respond(w, r, api.SuccessWithData("Article updated successfully", data))
	return
}
