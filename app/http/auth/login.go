package auth

import (
	"net/http"
	"strings"

	"github.com/gouniverse/api"
	"github.com/gouniverse/password"
	"sinevia.com/app/db"
	"sinevia.com/app/helpers"
)

// Login authenticates the login user
func Login(w http.ResponseWriter, r *http.Request) {
	// helpers.Respond(w, api.SuccessWithData("The API is working", map[string]interface{}{
	// 	"environment": helpers.AppEnv(),
	// }))

	email := strings.Trim(r.FormValue("email"), " ")
	pass := strings.Trim(r.FormValue("password"), " ")
	// callback := strings.Trim(r.URL.Query().Get("callback"), "")
	if email == "" {
		api.Respond(w, r, api.Error("Email is required field"))
		return
	}
	if pass == "" {
		api.Respond(w, r, api.Error("Password is required field"))
		return
	}

	user := db.UserFindByEmail(email)

	if user == nil {
		api.Respond(w, r, api.Error("This email is not registered"))
		return
	}

	if user["Email"] != email {
		api.Respond(w, r, api.Error("Email does not match"))
		return
	}

	isOk := password.Verify(pass, user["Password"])

	if isOk == false {
		api.Respond(w, r, api.Error("Password does not match"))
		return
	}

	// api.Respond(w, r, api.Error("TODO"))

	// if user.Status == models.USER_STATUS_EMAIL_UNVERIFIED {
	// 	emailVerificationToken := uid.NanoUid()
	// 	models.CacheSet(emailVerificationToken, user.Email, 3600)
	// 	emailVerificationURL := helpers.LinkAuthEmailVerify(emailVerificationToken)

	// 	to := []string{user.Email}
	// 	msg := helpers.EmailVerificationTemplate(user.FirstName+" "+user.LastName, emailVerificationURL)
	// 	isSent, _ := helpers.EmailSend("info@sinevia.com", to, "Account Verification", msg)
	// 	if isSent {
	// 		api.Respond(w, r, api.Error("Account unverified. Check your email for verification link"))
	// 	} else {
	// 		api.Respond(w, r, api.Error("Account unverified. Please contact the administrator"))
	// 	}
	// 	return
	// }

	userToken := helpers.RandStr(16)
	db.CacheSet(userToken, user["Id"], 3600)

	// var user = models.User{FirstName: sql.NullString{String: firstName, Valid: true}, LastName: sql.NullString{String: lastName, Valid: true}, Email: email, Password: sql.NullString{String: passwordHash, Valid: true}}

	delete(user, "Password")

	data := map[string]interface{}{
		"token": userToken,
		"user":  user,
	}

	api.Respond(w, r, api.SuccessWithData("Login successful", data))
	return
}
